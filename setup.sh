# parameters
VISUAL_STUDIO_CODE_VERSION="1.39.2"
PYTHON_VERSION="3.7.4"
PYTHON_PACKAGES=(
    "numpy"
    "scipy"
    "matplotlib"
    "ipython"
    "sympy"
    "pandas"
    "h5py"
    "jupyter"
    "scikit-learn"
    "click"
    "tqdm"
    "seaborn"
    "pillow"
    "sphinx"
    "requests"
)

# visual studio code
appdir="${HOME}/Applications/visual_studio_code"
versiondir="${appdir}/${VISUAL_STUDIO_CODE_VERSION}"
if [ ! -d "${versiondir}" ]; then
    mkdir -p "${appdir}"
    cd "${appdir}"
    curl -L "https://update.code.visualstudio.com/${VISUAL_STUDIO_CODE_VERSION}/linux-x64/stable" --output "${VISUAL_STUDIO_CODE_VERSION}.tar.gz"
    tar xf "${VISUAL_STUDIO_CODE_VERSION}.tar.gz"
    rm "${VISUAL_STUDIO_CODE_VERSION}.tar.gz"
    mv "VSCode-linux-x64" "${VISUAL_STUDIO_CODE_VERSION}"
    echo "export PATH=\"\${HOME}/Applications/visual_studio_code/${VISUAL_STUDIO_CODE_VERSION}/bin:\${PATH}\"" >> "${HOME}/.bashrc"
    cd "${OLDPWD}"

    source "${HOME}/.bashrc"
fi


# pyenv
if [ ! -d "${HOME}/.pyenv" ]; then
    curl https://pyenv.run | bash
cat << 'EOD' >> "${HOME}/.bashrc"
export PATH="${HOME}/.pyenv/bin:${PATH}"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
EOD
    
    source "${HOME}/.bashrc"
fi

# python
pyenv install "${PYTHON_VERSION}"
pyenv global "${PYTHON_VERSION}"

# python packages
pip install --upgrade pip setuptools
pip install --upgrade "${PYTHON_PACKAGES[@]}"

# Finish
echo "################################################################################"
echo "#                            INSTALLATION  COMPLETE                            #"
echo "################################################################################"

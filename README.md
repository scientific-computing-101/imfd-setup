IMFD Setup
==========

To setup the IMFD computers,

1. log into your account,
2. open a terminal, and
3. execute the command
   ```shell
   curl https://gitlab.com/scientific-computing-101/imfd-setup/raw/master/setup.sh | bash
   ```

Wait until the command has finished.
